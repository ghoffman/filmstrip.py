#!/usr/bin/env python

# You can use this to make a filmstrip from a movie file using ffmpeg and PIL(low)
# If you call it with a movie file ("-m"), it extracts images, by default at 2fps
#
# filmstrip.py -m /Path/To/Movie.mov
# 
# Then you can choose which frames should be in the filmstrip like so: 
#
# filmstrip.py  4 6 7
# 
# You can also combine these two operations: 
#
# filmstrip.py -m /Path/To/Movie.mov 2 4 9 12
#
# By default, all files are created in the current directory, 
# but you can specify them to go to another one
#

from PIL import Image
import sys, os
import argparse
from subprocess import call


parser = argparse.ArgumentParser()
parser.add_argument("-m","--movie", help="Input movie file")
parser.add_argument("-f","--framerate", help="Movie sample framerate (default 2)", default="2")
parser.add_argument("-w","--workdir", help="Working Directory (default .)", default="./")
parser.add_argument('frames', metavar='Frame#', nargs='*',
                   help='frames to pick for the filmstrip')
args = parser.parse_args()

if (args.movie):
	cmd = ["ffmpeg", "-i", args.movie,  "-r", args.framerate, args.output + "frame-%03d.jpeg"]
#	os.chdir(args.output) 
	call(cmd)
else:
	if (not args.frames):
		print "You need to either specify a movie or a list of frames (or both)"
		print
		parser.print_help()

if (not args.frames):
	exit()

BORDER = 5
COLOR = (255,255,255)

frames = ['frame-'+x.zfill(3)+'.jpeg' for x in args.frames]
images = []

for f in frames:
	images.append(Image.open(args.output+f))

w = images[0].size[0]
h = images[0].size[1]
n = len(images)

strip = Image.new('RGB',(w*n+BORDER*(n+1),h+BORDER*2),COLOR)

i = 0 
for im in images:
	strip.paste(im, (BORDER*(i+1)+w*i, BORDER))
	i += 1

strip.save(args.output+"filmstrip.jpeg")

